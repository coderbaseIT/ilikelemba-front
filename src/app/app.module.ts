import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MembreModule } from './modules/membre/membre.module';
import { AdministrateurModule } from './modules/administrateur/administrateur.module';
import { MainComponent } from './v2/components/main/main.component';
import { HeaderComponent } from './v2/components/header/header.component';
import { FooterComponent } from './v2/components/footer/footer.component';
import { BannerComponent } from './v2/components/banner/banner.component';
import { ParrainerComponent } from './v2/components/parrainer/parrainer.component';
import { InscriptionComponent } from './v2/components/inscription/inscription.component';
import { CreateBrancheComponent } from './v2/components/create-branche/create-branche.component';
import { AccueilSectionComponent } from './v2/components/accueil-section/accueil-section.component';
import { AboutSectionComponent } from './v2/components/about-section/about-section.component';
import { LoginComponent } from './v2/components/login/login.component';
import { MonCompteSectionComponent } from './v2/components/mon-compte-section/mon-compte-section.component';
import { BrancheBoxComponent } from './v2/components/branche-box/branche-box.component';
import { ResumeBrancheComponent } from './v2/components/resume-branche/resume-branche.component';
import { SharedModuleModule } from './modules/shared-module/shared-module.module';
import { ConfirmDialogComponent } from './dialogs/confirm-dialog/confirm-dialog.component';
import { ErrorDialogComponent } from './dialogs/error-dialog/error-dialog.component';
import { InformationDialogComponent } from './dialogs/information-dialog/information-dialog.component';
import { SuccesDialogComponent } from './dialogs/succes-dialog/succes-dialog.component';
import { WaitingDialogComponent } from './dialogs/waiting-dialog/waiting-dialog.component';
import { ConverterPipe } from './pipes/converter.pipe';
import { LoginAdaminComponent } from './v2/components/login-adamin/login-adamin.component';
import { MainAdaminComponent } from './v2/components/main-adamin/main-adamin.component';
import { MainAdaminNavComponent } from './v2/components/main-adamin-nav/main-adamin-nav.component';
import { PaiementComponent } from './v2/components/paiement/paiement.component';
import { RetraitComponent } from './v2/components/retrait/retrait.component';
import { PaiementAdminComponent } from './v2/components/paiement-admin/paiement-admin.component';
import { ConsultPaiementDialogComponent } from './dialogs/consult-paiement-dialog/consult-paiement-dialog.component';
import { TransfertComponent } from './v2/components/transfert/transfert.component';
import { ApprovisionnementComponent } from './v2/components/approvisionnement/approvisionnement.component';
import { DashboardComponent } from './v2/components/dashboard/dashboard.component';
import { CompteComponent } from './v2/components/compte/compte.component';
import { RapportApproComponent } from './v2/components/rapport-appro/rapport-appro.component';
import { PercentPipe } from './pipes/percent.pipe';
import { ApproAgentComponent } from './v2/components/appro-agent/appro-agent.component';
import { ActivationManagerComponent } from './v2/components/activation-manager/activation-manager.component';
import { MesActivationsComponent } from './v2/components/mes-activations/mes-activations.component';
import { MesPaiementsComponent } from './v2/components/mes-paiements/mes-paiements.component';
import { ConfigMembreComponent } from './v2/config-membre/config-membre.component';
import { ConfigRoleMembreComponent } from './v2/config-role-membre/config-role-membre.component';
import { NombreFilleulsActivesPipe } from './pipes/nombre-filleuls-actives.pipe';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    HeaderComponent,
    FooterComponent,
    BannerComponent,
    ParrainerComponent,
    InscriptionComponent,
    CreateBrancheComponent,
    AccueilSectionComponent,
    AboutSectionComponent,
    LoginComponent,
    MonCompteSectionComponent,
    BrancheBoxComponent,
    ResumeBrancheComponent,
    ConfirmDialogComponent,
    ErrorDialogComponent,
    InformationDialogComponent,
    SuccesDialogComponent,
    WaitingDialogComponent,
    ConverterPipe,
    LoginAdaminComponent,
    MainAdaminComponent,
    MainAdaminNavComponent,
    PaiementComponent,
    RetraitComponent,
    PaiementAdminComponent,
    ConsultPaiementDialogComponent,
    TransfertComponent,
    ApprovisionnementComponent,
    DashboardComponent,
    CompteComponent,
    RapportApproComponent,
    PercentPipe,
    ApproAgentComponent,
    ActivationManagerComponent,
    MesActivationsComponent,
    MesPaiementsComponent,
    ConfigMembreComponent,
    ConfigRoleMembreComponent,
    NombreFilleulsActivesPipe
  ],
  entryComponents:[
    ConsultPaiementDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModuleModule,
    //MembreModule,
    AdministrateurModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
