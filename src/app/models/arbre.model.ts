export class Arbre {
    public code: string;
    public fkGeniteur: number;
    public fkCategorieAdhesion: number;
    public statut: boolean;
    public dateCreat: Date;
    public montant: number;
    public description: string;
    public fkPartage: number;
    public fkDevise: number;

}
