export class MouvementCompte {
    public id: number;
    public fkCompte: number;
    public fkMembre: number;
    public debitUSD: number;
    public creditUSD: number;
    public soldeUSD: number;
    public debitCDF: number;
    public creditCDF: number;
    public soldeCDF: number;
    public statut: boolean;
    public dateCreat: Date;
}
