import { Administrateur } from './administrateur.model';
import { Membre } from './membre.model';
import { MembreAdapter } from './utilities/membre-adapter';

export class Paiement {
    id: number;
    fkMembre: number;
    fkAdmin: string;
    montantCDF: number;
    montantUSD: number;
    datePaiement: Date;
    dateDemande: Date;
    dateCreat: Date;
    statut: boolean;
    isAgent: boolean;
    fkManager: number
    fraisUSD: number;
    fraisCDF: number;
    montantTotalUSD: number;
    montantTotalCDF: number;
    etat: string;

    membre: Membre;
    admin:Administrateur;
    manager: Membre;

}
