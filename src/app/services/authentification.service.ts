import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpURLs } from '../utilities/http-urls';
import { HttpDataResponse } from '../utilities/http-data-response';
import { MembreAdapter } from '../models/utilities/membre-adapter';
import { ErrorResponse } from '../utilities/error-response';
import { AppConst } from '../utilities/app-const';
import { UserConnectedGuard } from '../guards/user-connected.guard';
import { AppConfigService } from './app-config.service';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfigService
  ) { }

  onAuthentification(data : any): Promise<HttpDataResponse<MembreAdapter>>{
    return new Promise<HttpDataResponse<MembreAdapter>>((resolve, reject) => {
      this.appConfig.onStartWaiting('Authentification en cours...');
      this.httpClient.post(HttpURLs.URL_LOGIN, JSON.stringify(data)).subscribe(
        (result: HttpDataResponse<MembreAdapter>) =>{
          console.log(result);
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          
          resolve(result)
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR)
        }
      )
    });
  }
}
