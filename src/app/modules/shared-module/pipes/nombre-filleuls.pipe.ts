import { Pipe, PipeTransform } from '@angular/core';
import { Membre } from 'src/app/models/membre.model';

@Pipe({
  name: 'nombreFilleuls'
})
export class NombreFilleulsPipe implements PipeTransform {

  transform(value: Membre[], ...args: unknown[]): number {
    let nbreFilleuls = 0;
    if(value){
      nbreFilleuls = value.length;
    }
    return nbreFilleuls;
  }

}
