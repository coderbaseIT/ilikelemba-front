import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'statutMembre'
})
export class StatutMembrePipe implements PipeTransform {

  transform(value: boolean, ...args: unknown[]): string {
    if(value === true){
      return "ACTIVE";
    }else{
      return "NON-ACTIVE"
    }
  }

}
