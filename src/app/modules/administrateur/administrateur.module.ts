import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdministrateurRoutingModule } from './administrateur-routing.module';
import { LoginComponent } from './components/login/login.component';
import { SharedModuleModule } from '../shared-module/shared-module.module';
import { FormsModule } from '@angular/forms';
import { MainAdminNavComponent } from './components/main-admin-nav/main-admin-nav.component'
import { SearchMembreComponent} from './components/search-membre/search-membre.component';
import { MainAdminComponent } from './components/main-admin/main-admin.component';
import { ConsultParrainageDialogComponent } from './components/consult-parrainage-dialog/consult-parrainage-dialog.component';
import { MembreModule } from '../membre/membre.module';
import { RapportAgentComponent } from './components/rapport-agent/rapport-agent.component';


@NgModule({
  declarations: [LoginComponent, MainAdminNavComponent, SearchMembreComponent, ConsultParrainageDialogComponent, RapportAgentComponent],
  exports: [LoginComponent, MainAdminNavComponent, SearchMembreComponent, ConsultParrainageDialogComponent, RapportAgentComponent],
  imports: [
    CommonModule,
    //AdministrateurRoutingModule,
    SharedModuleModule,
    MembreModule
  ],
  entryComponents:[
    ConsultParrainageDialogComponent
  ]
})
export class AdministrateurModule { }
