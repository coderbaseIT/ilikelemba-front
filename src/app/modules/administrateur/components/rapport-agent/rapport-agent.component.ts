import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Administrateur } from 'src/app/models/administrateur.model';
import { Paiement } from 'src/app/models/paiement';
import { ParrainageAdapter } from 'src/app/models/utilities/parrainage-adapter';
import { RapportAdapter } from 'src/app/models/utilities/rapport-adapter';
import { AdministrateurServiceService } from 'src/app/services/administrateur-service.service';
import { AppConfigService } from 'src/app/services/app-config.service';
import { HttpDataResponse } from 'src/app/utilities/http-data-response';
import { ConsultParrainageDialogComponent } from '../consult-parrainage-dialog/consult-parrainage-dialog.component';

@Component({
  selector: 'app-rapport-agent',
  templateUrl: './rapport-agent.component.html',
  styleUrls: ['./rapport-agent.component.css']
})
export class RapportAgentComponent implements OnInit {

  data = {
    debut: undefined,
    fin: undefined,
    fkAgent: undefined
  }

  totalActivationCDF: number = 0;
  totalActivationUSD: number = 0;

  totalPaiementCDF: number = 0;
  totalPaiementUSD: number = 0;

  waiting: boolean = false;
  parrainages: ParrainageAdapter[]

  rapport: RapportAdapter;

  subscription: Subscription;

  admins: Administrateur[];

  constructor(
    public service: AdministrateurServiceService,
    private appConfig: AppConfigService,
    private dialog: MatDialog,
    private router: Router
  ) { 
    
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {
    if(!this.service.userConnected){
      this.router.navigate(['admin/']);
    }
   this.subscription= this.appConfig.waiting$.subscribe(
      (result: boolean) =>{
        this.waiting = result;
      }
    );

    if(this.service.userConnected.role != 'agent'){
      this.service.getAllAdmins().then( res => this.admins = res.response);
    }
  }

  consultMembre(index: number): void{
    let dialogRef = this.dialog.open<ConsultParrainageDialogComponent, ParrainageAdapter>(ConsultParrainageDialogComponent,
      {
        width: '80%',
        height: '80%',
        data: this.parrainages[index]
      });

    dialogRef.afterClosed().subscribe(
      data =>{
        if(data){
          this.router.navigate(['/admin/main']);
        }
      }
    )
  }

  onSubmit(): void{
    if(this.service.userConnected.role == 'agent')
      this.data.fkAgent = this.service.userConnected.id;
    this.parrainages = []
    this.appConfig.onStartWaiting();
    this.service.rapport(JSON.stringify(this.data)).then(
      (result: HttpDataResponse<RapportAdapter>) => {
        this.appConfig.onStopWaiting();
        this.rapport = result.response;

        if(this.rapport.parrainages){
          this.totalActivationCDF = this.rapport.parrainages.filter(item => item.fkArbre.devise.id === 1).reduce<number>(
            (previous: number, current: ParrainageAdapter) =>{
              previous += current.fkArbre.montant;
  
              return previous;
            },0
          )
  
          this.totalActivationUSD = this.rapport.parrainages.filter(item => item.fkArbre.devise.id === 2).reduce<number>(
            (previous: number, current: ParrainageAdapter) =>{
              previous += current.fkArbre.montant;
  
              return previous;
            },0
          )
        }

        if(this.rapport.paiements){
          this.totalPaiementCDF = this.rapport.paiements.reduce<number>(
            (previous: number, current: Paiement) =>{
              previous += current.montantCDF;
  
              return previous;
            },0
          )
          this.totalPaiementUSD = this.rapport.paiements.reduce<number>(
            (previous: number, current: Paiement) =>{
              previous += current.montantUSD;
  
              return previous;
            },0
          )
        }
      }
    ).catch(error => {
      this.appConfig.onStopWaiting();
      alert(error)})
  }

  

}
