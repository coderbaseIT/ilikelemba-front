import { Component, OnInit } from '@angular/core';
import { GestionFilleulService } from 'src/app/services/gestion-filleul.service';
import { HttpDataResponse } from 'src/app/utilities/http-data-response';
import { NgModel } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MembreAdapter } from 'src/app/models/utilities/membre-adapter';
import { LoginDialogComponent } from 'src/app/modules/membre/components/login-dialog/login-dialog.component';
import { AppConfigService } from 'src/app/services/app-config.service';
import { AuthentificationService } from 'src/app/services/authentification.service';
import { AdministrateurServiceService } from 'src/app/services/administrateur-service.service';
import { Administrateur } from 'src/app/models/administrateur.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  data = {
    username: '',
    password: ''
  }

  waiting: boolean = false;
  waitingSubscription: Subscription;

  constructor(
    private authentService: AdministrateurServiceService,
    private  appConfig: AppConfigService,
    private router: Router
  ) { }
  ngOnDestroy(): void {
    this.waitingSubscription.unsubscribe();
  }

  

  ngOnInit(): void {
    this.waitingSubscription = this.appConfig.waiting$.subscribe(
      (next: boolean) => this.waiting = next
    )
  }

  onSubmit():void{
    this.appConfig.onStartWaiting();
    this.authentService.connexion(JSON.stringify(this.data)).then(
      (result: HttpDataResponse<Administrateur>) =>{
        this.appConfig.onStopWaiting();
        this.router.navigate(['/admin/main'])
      }
    ).catch(error => {
      alert(error);
      this.appConfig.onStopWaiting();
    })
  }


}
