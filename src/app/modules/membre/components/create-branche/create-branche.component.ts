import { Component, OnInit } from '@angular/core';
import { Devise } from 'src/app/models/devise';
import { Arbre } from 'src/app/models/arbre.model';
import { AppConfigService } from 'src/app/services/app-config.service';
import { GestionFilleulService } from 'src/app/services/gestion-filleul.service';
import { HttpDataResponse } from 'src/app/utilities/http-data-response';
import { NgForm } from '@angular/forms';
import { ArbreAdapter } from 'src/app/models/utilities/arbre-adapter';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { MessageDialogComponent } from 'src/app/modules/shared-module/dialogs/message-dialog/message-dialog.component';

@Component({
  selector: 'app-create-branche',
  templateUrl: './create-branche.component.html',
  styleUrls: ['./create-branche.component.css']
})
export class CreateBrancheComponent implements OnInit {

  devises: Devise[] = [
    new Devise()
  ]
  arbre: Arbre = new Arbre();

  constructor(
    private appConfig: AppConfigService,
    private filleulService: GestionFilleulService,
    private router: Router,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.arbre.fkDevise = undefined;
    this.appConfig.verifyUser();
    // if(!this.appConfig.userConnected.nom){
    //   this.router.navigate(['main']);
    // }
    this.appConfig.getDevise().then(
      (result: HttpDataResponse<Devise[]>) =>{
        this.devises = result.response;
      }
    ).catch(error => console.log(error))
  }

  onSubmit(form: NgForm): void{
    this.arbre.fkGeniteur = this.appConfig.userConnected.id;
    this.arbre.fkPartage = 1;
    console.log(this.arbre);
    this.filleulService.createArbre(this.arbre).then(
      (result: HttpDataResponse<ArbreAdapter>)=>{
          form.resetForm();
          this.appConfig.userConnected.arbres.push(result.response);
          this.dialog.open(MessageDialogComponent, {
            data: result.error.errorDescription
          })
          this.router.navigate(['main/main-filleul']);
      }
    ).catch(error => {
      this.dialog.open(MessageDialogComponent, {
        data: error
      })
    })
  }

}
