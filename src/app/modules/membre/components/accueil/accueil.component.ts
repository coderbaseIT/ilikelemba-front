import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LoginDialogComponent } from '../login-dialog/login-dialog.component';
import { MembreAdapter } from 'src/app/models/utilities/membre-adapter';
import { AppConfigService } from 'src/app/services/app-config.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit, OnDestroy {

  userConnected: MembreAdapter;
  userSubscription: Subscription
  constructor(
    private dialog: MatDialog,
    private appConfig: AppConfigService
  ) {
  if(appConfig.userConnected.nom)
    this.userConnected = appConfig.userConnected;
   }
  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.userSubscription = this.appConfig.userConnected$.subscribe(
      (next: MembreAdapter) => this.userConnected = next
    )
  }

  launchDialog(): void{
      let dialogRef = this.dialog.open(LoginDialogComponent, {
        width: '80%',
        height: '80%'
      })
  }

}
