import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ArbreAdapter } from 'src/app/models/utilities/arbre-adapter';
import { Membre } from 'src/app/models/membre.model';
import { AppConfigService } from 'src/app/services/app-config.service';

@Component({
  selector: 'app-branche-box',
  templateUrl: './branche-box.component.html',
  styleUrls: ['./branche-box.component.css']
})
export class BrancheBoxComponent implements OnInit, OnChanges {

  @Input() arbre: ArbreAdapter = new ArbreAdapter();

  g2: Membre[] = []
  g3: Membre[] = []

  constructor(
    private appConfig: AppConfigService
  ) { }
  ngOnChanges(changes: SimpleChanges): void {
    this.g2 = [];
    this.g3 = [];
  }

  ngOnInit(): void {
    this.g2 = this.arbre.g2;
    this.g3 = this.arbre.g3;
  }

  onFilleul1Selected(id: number): void{
    this.g2 = [];
    this.g3 = [];
    this.g2 = this.arbre.g2.filter(membre => membre.fkParrain === id);
  }

  onFilleul2Selected(id: number): void{
    this.g3 = [];
    this.g3 = this.arbre.g3.filter(membre => membre.fkParrain === id);
  }

}
