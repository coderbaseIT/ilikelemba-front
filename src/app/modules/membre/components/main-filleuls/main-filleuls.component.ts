import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArbreAdapter } from 'src/app/models/utilities/arbre-adapter';
import { AppConfigService } from 'src/app/services/app-config.service';

@Component({
  selector: 'app-main-filleuls',
  templateUrl: './main-filleuls.component.html',
  styleUrls: ['./main-filleuls.component.css']
})
export class MainFilleulsComponent implements OnInit {

  arbres: ArbreAdapter[] = [];
  currentArbre: ArbreAdapter = new ArbreAdapter();

  constructor(
    private appConfig: AppConfigService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.appConfig.verifyUser();
    // if(!this.appConfig.userConnected.nom){
    //   this.router.navigate(['main']);
    // }
    this.arbres = this.appConfig.userConnected.arbres;
    this.currentArbre = this.arbres[0];
  }

  showArbre(arbre: ArbreAdapter):void{
    this.currentArbre = arbre;
  }

}
