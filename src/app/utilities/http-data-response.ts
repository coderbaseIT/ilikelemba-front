import { ErrorResponse } from './error-response';

export class HttpDataResponse<T> {
    error: ErrorResponse;
    response: T;
}
