export class AppConst {
    static NETWORK_ERROR = "Il semble y avoir un problème de réseau. Veuillez réessayer ou contacter votre fournisseur réseau";

    static FRAIS_RETRAIT_CODE = 'FRAIS_RETRAIT';
    static BENEFICE_MANAGER = "BENEFICE_MANAGER";
}
