export class ErrorResponse {
    errorCode: string;
    errorDescription: string;

    static KO: string = 'KO';
    static OK: string = 'OK';

    constructor() {
        this.errorCode = ErrorResponse.KO;
        this.errorDescription = '';
    }

    
}
