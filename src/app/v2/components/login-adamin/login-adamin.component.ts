import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Administrateur } from 'src/app/models/administrateur.model';
import { AdministrateurServiceService } from 'src/app/services/administrateur-service.service';
import { AppConfigService } from 'src/app/services/app-config.service';
import { HttpDataResponse } from 'src/app/utilities/http-data-response';

@Component({
  selector: 'app-login-adamin',
  templateUrl: './login-adamin.component.html',
  styleUrls: ['./login-adamin.component.css']
})
export class LoginAdaminComponent implements OnInit {

  data = {
    username: '',
    password: ''
  }

  waiting: boolean = false;
  waitingSubscription: Subscription;

  constructor(
    private authentService: AdministrateurServiceService,
    private  appConfig: AppConfigService,
    private router: Router
  ) { }
  ngOnDestroy(): void {
    this.waitingSubscription.unsubscribe();
  }

  

  ngOnInit(): void {
    this.waitingSubscription = this.appConfig.waiting$.subscribe(
      (next: boolean) => this.waiting = next
    )
  }

  onSubmit():void{
    this.appConfig.onStartWaiting();
    this.authentService.connexion(JSON.stringify(this.data)).then(
      (result: HttpDataResponse<Administrateur>) =>{
        this.appConfig.onStopWaiting();
        this.router.navigate(['/admin/main'])
      }
    ).catch(error => {
      alert(error);
      this.appConfig.onStopWaiting();
    })
  }

}
