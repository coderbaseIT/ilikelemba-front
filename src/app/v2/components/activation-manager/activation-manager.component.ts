import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ParrainageAdapter } from 'src/app/models/utilities/parrainage-adapter';
import { InputDialogComponent } from 'src/app/modules/shared-module/dialogs/input-dialog/input-dialog.component';
import { MessageDialogComponent } from 'src/app/modules/shared-module/dialogs/message-dialog/message-dialog.component';
import { AppConfigService } from 'src/app/services/app-config.service';
import { GestionFilleulService } from 'src/app/services/gestion-filleul.service';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-activation-manager',
  templateUrl: './activation-manager.component.html',
  styleUrls: ['./activation-manager.component.css']
})
export class ActivationManagerComponent implements OnInit {

  parrainages: ParrainageAdapter[] = []
  motclef: string = '';

  constructor(
    private service: GestionFilleulService,
    private dialog: MatDialog,
    private appConfig: AppConfigService
  ) { }

  ngOnInit(): void {
  }

  consultMembre(index: number): void{

  }

  searchParrainages(): void{
    this.service.searchParrainage(JSON.stringify({motclef: this.motclef})).then(
      res => this.parrainages = res.response
    ).catch(error => Utility.openInfoDialog(this.dialog, error))
  }

  filterData(): ParrainageAdapter[]{
    return this.parrainages.filter(e => (e.membre.nom.toLowerCase().includes(this.motclef.toLowerCase()) || e.membre.postnom.toLowerCase().includes(this.motclef.toLowerCase()) || e.membre.prenom.toLowerCase().includes(this.motclef.toLowerCase()) ||  e.membre.telephone.toLowerCase().includes(this.motclef.toLowerCase())) && !e.statut)
  }

  activer(parrainage: ParrainageAdapter): void{
    let dialogRef = this.dialog.open<InputDialogComponent>(InputDialogComponent, 
      {
        data: 'Veuillez inserer votre mot de passe'
      });
      dialogRef.afterClosed().subscribe(
        (password: string) =>{
          if(this.appConfig.userConnected.login.password === password){
            this.service.activerFilleul(JSON.stringify({id: parrainage.id, fkManager: this.appConfig.userConnected.id}))
            .then(
              res => {
                Utility.openSuccessDialog(this.dialog, 'Activation effectuée avec suucès');
                parrainage.statut = true;
              }
            )
            .catch(error => Utility.openInfoDialog(this.dialog, error))
          }else{
            this.dialog.open(MessageDialogComponent, {
              data: 'Le mot de passe fournit est incorrect'
            })
          }
        }
      )
  }

}
