import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Fade } from 'src/app/animations/fade';
import { Paiement } from 'src/app/models/paiement';
import { AppConfigService } from 'src/app/services/app-config.service';
import { GestionFilleulService } from 'src/app/services/gestion-filleul.service';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-paiement',
  templateUrl: './paiement.component.html',
  styleUrls: ['./paiement.component.css'],
  animations: [Fade]
})
export class PaiementComponent implements OnInit {

  paiements: Paiement[]

  constructor(
    private appConfig: AppConfigService,
    private dialog: MatDialog,
    private service: GestionFilleulService
  ) { }

  ngOnInit(): void {
    this.update()
  }

  annuler(value: Paiement): void{
    Utility.openConfirmDialog(this.dialog, 'Êtes-vous sûr de vouloir annuler ce paiements?')
    .afterClosed().
    subscribe(
      r => {
        if(r === 'OK'){
          this.service.annulerPaiement(JSON.stringify(value))
          .then(
            res => {
              Utility.openSuccessDialog(this.dialog, res.error.errorDescription)
              this.update()
            }
          )
        }
      }
    )
  }

  update(): void{
    this.service.getListPaiements(this.appConfig.userConnected.id).then(
      res => this.paiements = res.response
    ).catch(
      error => Utility.openErrorDialog(this.dialog, error)
    )
  }

}
