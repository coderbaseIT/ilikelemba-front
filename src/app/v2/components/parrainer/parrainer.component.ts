import { Component, OnInit } from '@angular/core';
import { Fade } from 'src/app/animations/fade';

@Component({
  selector: 'app-parrainer',
  templateUrl: './parrainer.component.html',
  styleUrls: ['./parrainer.component.css'],
  animations: [Fade]
})
export class ParrainerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  codeCountry: string = "+243";
  onCountrySelected(value: string): void{
    this.codeCountry = value;
  }

}
