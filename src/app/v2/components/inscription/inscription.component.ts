import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Fade } from 'src/app/animations/fade';
import { Devise } from 'src/app/models/devise';
import { ArbreAdapter } from 'src/app/models/utilities/arbre-adapter';
import { MembreAdapter } from 'src/app/models/utilities/membre-adapter';
import { MessageDialogComponent } from 'src/app/modules/shared-module/dialogs/message-dialog/message-dialog.component';
import { AppConfigService } from 'src/app/services/app-config.service';
import { GestionFilleulService } from 'src/app/services/gestion-filleul.service';
import { HttpDataResponse } from 'src/app/utilities/http-data-response';
import { Util } from 'src/app/utilities/util';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css'],
  animations: [Fade]
})
export class InscriptionComponent implements OnInit {

  membre: MembreAdapter = new MembreAdapter();
  arbres: ArbreAdapter[];
  devise: string = '';
  branche: string;

  isChecked:boolean = false;
  confirmPassword: string = '';

  devises: Devise[];

  constructor(
    private service: GestionFilleulService,
    private activatedRoute: ActivatedRoute,
    private appConfig: AppConfigService,
    private dialog: MatDialog,
    private router: Router
  ) { 
    this.membre.sexe = 'M',
    this.appConfig.getArbres().then(
      result => {
        this.arbres = null;
        this.arbres = result.response
      }
    ).catch()
  }

  

  ngOnInit(): void {
    console.log(this.activatedRoute.snapshot.queryParams);
    this.membre.fkParrain = this.activatedRoute.snapshot.queryParams.mli;
    this.membre.fkArbre = this.activatedRoute.snapshot.queryParams.branche;

    this.branche = this.activatedRoute.snapshot.queryParams.branche;

    this.appConfig.getDevise().then(
      (result: HttpDataResponse<Devise[]>) =>{
        this.devises = result.response;
      }
    ).catch()
  }

  codeCountry: string = "+243";
  telephone:string = ''
  onCountrySelected(value: string): void{
    this.codeCountry = value;
  }

  onSubmit(): void{
    this.telephone = Utility.verify(this.telephone);
    this.membre.telephone = this.codeCountry + this.telephone;

    if(this.telephone.length !== 9){
      Utility.openInfoDialog(this.dialog, 'Le numéro de téléphone incorrect.');
      return;
    }

    if(!Utility.verifyEmail(this.membre.email)){
      Utility.openInfoDialog(this.dialog, "L'adresse-mail saisie est incorrecte");
      return;
    }

    if(!this.membre.login.password || this.membre.login.password.length < 8){
      Utility.openInfoDialog(this.dialog, "Les mots de passe doit comporter au moins 8 caractères");
      return;
    }

    if(this.membre.login.password !== this.confirmPassword){
      Utility.openInfoDialog(this.dialog, "Les mots de passe saisits sont différents");
      return;
    }

    this.service.inscription(JSON.stringify(this.membre)).then(
      result => {
        Utility.openSuccessDialog(this.dialog, result.error.errorDescription);
        this.router.navigate(['/login'], { replaceUrl: true })
      }
    ).catch(error => {
      Utility.openInfoDialog(this.dialog, error)
    })
  }

  onSelect(value: string): void{
    this.membre.fkArbre = value
  }

  isPasswordCorrect(): boolean{
    return this.membre.login.password === this.confirmPassword
  }

  isEmailCorrect(): boolean{
    return Utility.verifyEmail(this.membre.email);
  }

  arbreSelect(): string{
    let data: string = '';
    let arbre: ArbreAdapter;

    if(this.membre.fkArbre && this.arbres){
      arbre = this.arbres.filter(e => this.membre.fkArbre === e.code)[0];

      try {
        data = arbre.description + ' (' + arbre.montant + ' ' + arbre.devise.code+' ) ' ; 
      } catch (error) {
        
      }

    }

    return data;
  }

  onDeviseChange(): ArbreAdapter[]{
    if(!this.arbres) return;
    return this.arbres.filter(e => this.devise === e.devise.code);
  }

}
