import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Fade } from 'src/app/animations/fade';
import { Arbre } from 'src/app/models/arbre.model';
import { Devise } from 'src/app/models/devise';
import { ArbreAdapter } from 'src/app/models/utilities/arbre-adapter';
import { MessageDialogComponent } from 'src/app/modules/shared-module/dialogs/message-dialog/message-dialog.component';
import { AppConfigService } from 'src/app/services/app-config.service';
import { GestionFilleulService } from 'src/app/services/gestion-filleul.service';
import { HttpDataResponse } from 'src/app/utilities/http-data-response';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-create-branche',
  templateUrl: './create-branche.component.html',
  styleUrls: ['./create-branche.component.css'],
  animations: [Fade]
})
export class CreateBrancheComponent implements OnInit {
  devises: Devise[]
  arbre: Arbre = new Arbre();

  constructor(
    private appConfig: AppConfigService,
    private filleulService: GestionFilleulService,
    private router: Router,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.arbre.fkDevise = 0;
    this.appConfig.getDevise().then(
      (result: HttpDataResponse<Devise[]>) =>{
        this.devises = result.response;
      }
    ).catch(error => console.log(error))
  }

  onSubmit(form: NgForm): void{
    this.arbre.fkGeniteur = this.appConfig.userConnected.id;
    this.arbre.fkPartage = 1;

    console.log(this.arbre)

    if(this.arbre.fkDevise == 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez selectionner le devise');
      return;
    }

    this.filleulService.createArbre(this.arbre).then(
      (result: HttpDataResponse<ArbreAdapter>)=>{
          form.resetForm();
          this.appConfig.userConnected.arbres.push(result.response);
          Utility.openSuccessDialog(this.dialog, result.error.errorDescription);
          this.router.navigate(['myaccount'])
      }
    ).catch(error => {
      Utility.openInfoDialog(this.dialog, error);
    })
  }

}
