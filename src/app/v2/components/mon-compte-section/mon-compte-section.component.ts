import { Component, OnInit } from '@angular/core';
import { Fade } from 'src/app/animations/fade';
import { ArbreAdapter } from 'src/app/models/utilities/arbre-adapter';
import { AppConfigService } from 'src/app/services/app-config.service';
import { Router } from '@angular/router';
import { MembreAdapter } from 'src/app/models/utilities/membre-adapter';

@Component({
  selector: 'app-mon-compte-section',
  templateUrl: './mon-compte-section.component.html',
  styleUrls: ['./mon-compte-section.component.css'],
  animations: [Fade]
})
export class MonCompteSectionComponent implements OnInit {

  arbres: ArbreAdapter[];
  currentArbre: ArbreAdapter;

  userConnected: MembreAdapter

  constructor(
    public appConfig: AppConfigService,
    private router: Router
  ) { 
    this.userConnected = appConfig.userConnected;
  }

  ngOnInit(): void {
    this.arbres = this.appConfig.userConnected.arbres;
    this.currentArbre = this.arbres[0];
  }

  showArbre(arbre: ArbreAdapter):void{
    this.currentArbre = arbre;
  }

}
