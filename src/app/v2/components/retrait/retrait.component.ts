import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Fade } from 'src/app/animations/fade';
import { Arbre } from 'src/app/models/arbre.model';
import { Config } from 'src/app/models/config'
import { Devise } from 'src/app/models/devise';
import { Paiement } from 'src/app/models/paiement';
import { AppConfigService } from 'src/app/services/app-config.service';
import { GestionFilleulService } from 'src/app/services/gestion-filleul.service';
import { AppConst } from 'src/app/utilities/app-const';
import { HttpDataResponse } from 'src/app/utilities/http-data-response';
import { Util } from 'src/app/utilities/util';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-retrait',
  templateUrl: './retrait.component.html',
  styleUrls: ['./retrait.component.css'],
  animations: [Fade]
})
export class RetraitComponent implements OnInit {

  devises: Devise[];
  paiement: Paiement = new Paiement();
  config: Config

  devise: string = '0';
  montant: number = 0;

  type: string = 'MANAGER'

  constructor(
    public appConfig: AppConfigService,
    private service: GestionFilleulService,
    private router: Router,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.paiement.isAgent = false;
    this.paiement.montantCDF = 0;
    this.paiement.montantUSD = 0;
    this.paiement.fkMembre = this.appConfig.userConnected.id;
    this.appConfig.getDevise().then(
      (result: HttpDataResponse<Devise[]>) =>{
        this.devises = result.response;
      }
    ).catch(error => console.log(error))
    this.appConfig.getConfig(JSON.stringify({code: AppConst.FRAIS_RETRAIT_CODE})).then(
      res => this.config = res.response
    ).catch(error => Utility.openInfoDialog(this.dialog, error))
  }

  onSubmit(): void{
    console.log('DEVISE',this.devise);
    console.log('MONTANT',this.montant);
    console.log('PAIEMENT',this.paiement);

    if(this.type == 'AGENT')
      this.paiement.isAgent = true;
    else
      this.paiement.isAgent = false;
    if(this.devise === 'USD')
      this.paiement.montantUSD = this.montant;
    if(this.devise === 'CDF') this.paiement.montantCDF = this.montant;
    
    if(this.paiement.isAgent && this.paiement.fkAdmin.length < 10){
      Utility.openInfoDialog(this.dialog, "Code de l'agent incorrect");
      return;
    }
    if(this.montant === 0){
      Utility.openInfoDialog(this.dialog, "Veuillez saisir le montant à retirer");
      return;
    }

    if(this.devise === 'USD' && this.appConfig.userConnected.compte.soldeUSD < ((this.montant * this.config.valueDecimal/100) + this.montant)){
      Utility.openInfoDialog(this.dialog, "Le solde de votre compte est insuffisant");
      return;
    }

    if(this.devise === 'CDF' && this.appConfig.userConnected.compte.soldeCDF < ((this.montant * this.config.valueDecimal/100) + this.montant)){
      Utility.openInfoDialog(this.dialog, "Le solde de votre compte est insuffisant");
      return;
    }

    Utility.openConfirmDialog(this.dialog, 'Êtes-vous sûr de vouloir effectuer ce retrait?')
    .afterClosed()
    .subscribe(
      res => {
        if(res === 'OK'){
          this.service.effectuerPaiement(JSON.stringify(this.paiement))
          .then(
            result => {
              Utility.openSuccessDialog(this.dialog, result.error.errorDescription)
              this.appConfig.userConnected.compte.soldeCDF = this.appConfig.userConnected.compte.soldeCDF - this.paiement.montantCDF * this.config.valueDecimal /100;
              this.appConfig.userConnected.compte.soldeUSD = this.appConfig.userConnected.compte.soldeUSD - this.paiement.montantCDF * this.config.valueDecimal / 100;
            }
          ).catch(error => Utility.openErrorDialog(this.dialog, error))
        }
      }
    )

  }

}
