import { Component, OnInit, Input } from '@angular/core';
import { ArbreAdapter } from 'src/app/models/utilities/arbre-adapter';
import { AppConfigService } from 'src/app/services/app-config.service';
import { HttpURLs } from 'src/app/utilities/http-urls';

@Component({
  selector: 'app-branche-box',
  templateUrl: './branche-box.component.html',
  styleUrls: ['./branche-box.component.css']
})
export class BrancheBoxComponent implements OnInit {

  @Input() arbre: ArbreAdapter = new ArbreAdapter();
  user: number; 
  url: string = HttpURLs.getUrlInscription();
  mli: number;
  branche: string ;
  constructor(
    private appConfig: AppConfigService
  ) {
    this.mli = appConfig.userConnected.id;
    this.branche = this.arbre.code;
   }

  ngOnInit(): void {
    this.user = this.appConfig.userConnected.id;
  }

}
